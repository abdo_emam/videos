<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Category;
use App\Models\Skill;
use App\Models\Tag;
use App\Models\Comment;

class Video extends Model
{
    protected $fillable = [
        'name','meta_keywords','meta_des','des','youtube','published','user_id','cat_id','image'
    ];

         function user()
        {
            return $this->belongsTo('App\Models\User', 'user_id');
        }

        
       function cat()
        {
            return $this->belongsTo('App\Models\Category', 'cat_id');
        }

        
     public function skills()
     {
            return $this->belongsToMany('App\Models\Skill', 'skill_video');
        }
        
     public function tags()
     {
               return $this->belongsToMany('App\Models\Tag', 'tag_video');
        }    

    public function comments()
    {
           return $this->hasMany('App\Models\Comment');
            }   
    }

