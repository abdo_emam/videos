<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Video;

class Skill extends Model
{
    protected $fillable = ['name'];


    public function videos()
    {
        return $this->belongsToMany('App\Models\Video', 'skill_video');
    }


    }
    
