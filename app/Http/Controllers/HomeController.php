<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\FrontEnd\Comments\Store;
//use App\Http\Requests\FrontEnd\contacts\Store;
use App\Http\Requests\FrontEnd\users\UserStore;

use Illuminate\Http\Request;
use App\Models\Video;
use App\Models\Category;
use App\Models\Skill;
use App\Models\Tag;
use App\Models\Comment;
use App\Models\Message;
use App\Models\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['commentUpdate','deletComment','profileUpdate']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $videos = Video::orderBy('id','desc');
        if(request()->has('search') && request()->get('search') != '' ){
            $videos = $videos->where('name' , 'like' , "%".request()->get('search')."%");

        }

       $videos= $videos->paginate(30);
        return view('home', compact('videos'));
    }

    public function category($id)
    {
         $category = Category::findOrFail($id);
         $videos = Video::where('cat_id',$id)->orderBy('id','desc')->paginate(30);
        return view('front-end.category.index', compact('videos','category'));
    }

    public function skills($id)
    {
        $skill=Skill::findOrFail($id);

        $videos = Video::whereHas('skills',function($query) use ($id){
            $query->where('skill_id',$id);

        })->orderBy('id','desc')->paginate(30);

        return view('front-end.skill.index', compact('videos','skill'));

    }

    public function video($id)
    {
        $video = Video::with('cat','skills','tags','user','comments.user')->findOrFail($id);

        return view ('front-end.video.index',compact('video'));
    }

    public function tags($id)
    {
        $tag=Tag::findOrFail($id);

        $videos = Video::whereHas('tags',function($query) use ($id){
            $query->where('tag_id',$id);

        })->orderBy('id','desc')->paginate(30);

        return view('front-end.tag.index', compact('videos','tag'));

    }

    public function commentStore($id, Store $request)
    {
        $video = Video::findOrFail($id);
        $comment=Comment::create([
            'user_id'=>auth()->user()->id,
            'video_id'=>$video->id,
            'comment'=>$request->comment,

        ]);
       // dd($comment);

       return redirect()->route('front.video',['id'=>$video->id,'#comments']);

    
    }

     public function commentUpdate($id, Store $request)
     {
         $comment = Comment::findOrFail($id);
         if(($comment->user_id == auth()->user()->id) || auth()->user()->group=='admin' ){

        $comment->update(['comment' => $request->comment]);
      
         }

         return redirect()->route('front.video',['id'=>$comment->video_id,'#comments']);


     }

     /****delet comment */
     public function deletComment($id){
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return redirect()->route('front.video' , ['id' => $comment->video_id , '#comments']);

     }
     

     public function messageStor(Request $request)
      {
        Message::create([
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message,
      
      ]);
        return redirect()->route('frontend.landing');
      }

      public function welcome()
     {
        $videos = Video::orderBy('id','desc')->paginate(9);

        $count_videos = Video::count();
        $count_comments = Comment::count();
        $count_tags = Tag::count();

      return view ('welcome',compact('videos','count_videos','count_comments','count_tags'));
    }

    public function profile($id,$slug=null){
        $user = User::findOrFail($id);
       
         return view('front-end.profile.index',compact('user'));

     }
     /**updateprofile */
     public function profileUpdate(UserStore $request)
     {
        $user = User::findOrFail(auth()->user()->id);
        $array=[];
        if($request->email != $user->email){
            $email=User::where('email','$request->email')->first();
            
            if($email == null){
                $array['email']=$request->email;
            }
        }

        if($request->name != $user->name){
                
          $array['name']=$request->name;
        }
        if($request->password != $user->password){
                
            $array['password']=Hash::make($request->password);
        }

        if(!empty($array)){
            $user->update($array);
        }

        return redirect()->route('front.profile',['id'=>$user->id, 'slug'=>slug($user->name)]);





       

     }

 }

    

    // public function test()
    // {
    //   return view ('front-end.test');
    // }


