<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Skill;
class Skills extends BackEndController
{
    public function __construct(Skill $model)
    {
      parent::__construct($model);
  
    }
  
   
    
    public function store(Request $request){
      
        Skill::create([
        'name' => $request->name,

  ]);
  //dd($request->all());
   return redirect()->route('skills.index');
    }
  
  
  public function update($id,Request $request){
    $row =Skill::find($id);
    $requstArray=[
      'name' => $request->name,
     
    ];
    
  $row->update($requstArray);  
  return redirect()->route('skills.index');
  
  }
}
