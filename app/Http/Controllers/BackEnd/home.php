<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Comment;

class home extends BackEndController
{
    public function __construct(User $model)
    {
      parent::__construct($model);
  
    }

    public function index(){
      $comments = Comment::with('user' , 'video')->orderby('id' , 'desc')->get();
     // dd($comments);

         return view('back-end.home',compact('comments'));

    }
}
