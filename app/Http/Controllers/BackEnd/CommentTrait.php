<?php
namespace App\Http\Controllers\BackEnd;
use Illuminate\Http\Request;
use App\Models\Comment;
trait CommentTrait
{

    public function commentStore(Request $request){
        $requestArray = $request->all() + ["user_id" => auth()->user()->id];
        Comment::create($requestArray);
       // dd($requestArray);
        return redirect()->route('videos.edit' , ['id' => $requestArray['video_id'] ]);

    }

}