<?php
namespace App\Http\Controllers\BackEnd;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class BackEndController extends Controller
{
    protected $model;
    public function __construct (Model $model){
      $this->model=$model;
    }



public function index()
{
  $rows = $this->model;
  $rows = $this->filter($rows);
  $with = $this->with();
  if(!empty($with)){
      $rows = $rows->with($with);
  }
  $rows = $rows->paginate(10);


$moduleName=$this->pluralModelName();

$pageTitle="Control ".$moduleName;
$routeName=$this->getClassNameFromModel();
$pageDesc="here you can Add / Edit / Delet  ".$moduleName;
        return view('back-end.'.$this->getClassNameFromModel().'.index',compact(
          'rows',
        'moduleName',
        'pageTitle',
        'pageDesc',
        'routeName'
      ));
     
    }
    
  
    
////////////////////*C r e a te*///////////////////

    public function create(){
      $moduleName=$this->getModelName();

      $pageTitle="Create ".$moduleName;

      $pageDesc="here you can create  ".$moduleName;
      $folderName= $this->getClassNameFromModel();
      $routeName= $folderName;
      $append = $this->append();
      return view('back-end.' . $folderName . '.create' , compact(
        'pageTitle',
        'moduleName',
        'pageDesc',
        'folderName',
        'routeName'
    ))->with($append);
}

 /////////////*  E D  I T *//////////////////

    public function edit($id)
    {
    $row =$this->model->find($id);

    $moduleName=$this->getModelName();

    $pageTitle="Edit ".$moduleName;

    $pageDesc="here you can Edit  ".$moduleName;

    $folderName= $this->getClassNameFromModel();

    $routeName= $folderName;
    
    $append = $this->append();


      return view('back-end.'.$folderName.'.edit',compact(
        'row',
      'moduleName',
      'pageTitle',
      'pageDesc',
      'folderName',
      'routeName'))->with($append);
    }  

  ///////////////////*DELET*//////////////////////

    public function destroy($id){
       $this->model->find($id)->delete();
      return redirect()->route($this->getClassNameFromModel().'.index');
  
    }    

 /* Filters*/
  protected function filter($rows){
        return $rows;
    }


    protected function with(){
      return [];

    }
    
    
    protected function append(){
      return [];
  }

    public function getClassNameFromModel(){

     return strtolower ($this->pluralModelName());
     

    }

    protected function pluralModelName(){
      return str_plural($this->getModelName());

    }
    
    protected function getModelName(){
      return class_basename($this->model);

    }
}

