<?php

namespace App\Http\Controllers\BackEnd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Http\Controllers\BackEnd\BackEndController;

class Users extends BackEndController
{

  public function __construct(User $model)
  {
    parent::__construct($model);

  }

 /* public function index(){
    $rows = User::paginate(10);
    return view('back-end.users.index',compact('rows'));
  }*/

    
 /*public function create(){
   return view( 'back-end.users.create');
 }*/
  
  public function store(Request $request){
    
   User::create([
    'name' => $request->name,
    'email' => $request->email,
    'group' => $request->group,
    'password' => Hash::make($request->password),
]);
//dd($request->all());
 return redirect()->route('users.index');
  }

/*Update */
  /*public function edit($id){
      $row =User::find($id);
    return view('back-end.users.edit',compact('row'));
}  */

public function update($id,Request $request){
  $row =User::find($id);
  $requstArray=[
    'name' => $request->name,
    'email' => $request->email,
    'group' => $request->group,

  ];
  if (request()->has('password')&& $request->get('password')!='') {

    $requstArray=$requstArray +[ 'password' => Hash::make($request->password)];
  }
$row->update($requstArray);  
return redirect()->route('users.index');

}

/*public function destroy($id){
User::findorFail($id)->delete();
return redirect()->route('users.index');

}*/



}
