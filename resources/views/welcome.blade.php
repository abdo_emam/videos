@extends('layouts.app')

@section('content')
  @include('front-end.homepage-sections.home-image')
  @include('front-end.homepage-sections.videos')
  @include('front-end.homepage-sections.statistics')


  @include('front-end.homepage-sections.contact-us')

  
@endsection