<div class="section section-dark text-center">
        <div class="container">
          <h2 class="title">Our Numbers</h2>
          <div class="row">
            <div class="col-md-4">
              <div class="card  card-plain">
                <div class="card-body">
                  <a href="#paper-kit">
                    <div class="author">
                      <h2 class="card-title" style="font-size :50px">{{$count_videos}}</h2>
                      <h6 class="card-category">Videos</h6>
                    </div>
                  </a>
                  
                </div>
                
              </div>
            </div>
            <div class="col-md-4">
                    <div class="card  card-plain">
                      <div class="card-body">
                        <a href="#paper-kit">
                          <div class="author">
                            <h2 class="card-title" style="font-size :50px">{{$count_comments}}</h2>
                            <h6 class="card-category">Comments</h6>
                          </div>
                        </a>
                        
                      </div>
                      
                    </div>
                  </div>
                   <div class="col-md-4">
              <div class="card  card-plain">
                <div class="card-body">
                  <a href="#paper-kit">
                    <div class="author">
                      <h2 class="card-title" style="font-size :50px">{{$count_tags}}</h2>
                      <h6 class="card-category">Tags</h6>
                    </div>
                  </a>
                  
                </div>
                
              </div>
            </div>
                  
            
          </div>
          
          
        </div>
      </div>