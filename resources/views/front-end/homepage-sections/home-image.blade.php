<div class="page-header section-dark" style="background-image: url('{{asset('')}}/front-end/img/antoine-barres.jpg')">
    <div class="filter"></div>
    <div class="content-center">
      <div class="container">
        <div class="title-brand">
          <h1 class="presentation-title">5dmat-web.com</h1>
          <div class="fog-low">
            <img src="{{asset('')}}/front-end/img/fog-low.png" alt="">
          </div>
          <div class="fog-low right">
            <img src="{{asset('')}}/front-end/img/fog-low.png" alt="">
          </div>
        </div>
        <h2 class="presentation-subtitle text-center">Learn programming </h2>
      </div>
    </div>
    <div class="moving-clouds" style="background-image: url('front-end/img/clouds.png'); "></div>
    
  </div>