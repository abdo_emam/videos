<div class="card" style="width: 20rem;"> 
<a href="{{route('front.video',['id'=>$video->id])}}">
    <img class="card-img-top" src="{{url('uploads/'.$video->image)}}" alt=" {{$video->name}}" style="width : 320px;height:240px;">
  </a>
  <div class="card-body">
   <p class="card-text">
      <a href="{{route('front.video',['id'=>$video->id])}}">
         {{$video->name}}
      </a>
            
    </p>
    <small>{{$video->created_at}}</small>
  </div>
</div>