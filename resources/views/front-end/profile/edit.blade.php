
  <div class="card text-center" id="profile_card" style="display:none">
  <div class="card-header">
   <h4 style="margin-bottom:15px"> Update Profile</h4>
  </div>
  <div class="card-body">
           <form action="{{route('profile.update')}}" method="post">
            {{ csrf_field() }}
                <div class="form-group">
                  <label for="exampleFormControlInput1">Name</label>
                  <input type="text" class="form-control " name="name" value="{{isset($user)? $user->name : ''}}">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Email address</label>
                    <input type="email" class="form-control " name="email" value="{{isset($user)? $user->email : ''}}">
                </div>
                
                <div class="form-group">
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                </div>
                <button type="submit" class="btn btn">Edit</button>
              </form>
            
    
  </div>
</div>