<form method="post" action="{{ route('front.AddComment',['id'=> $video->id ]) }}">
    @csrf
    <div class="form-group">
        <textarea type="text" name="comment" class="form-control  @error('comment') is-invalid @enderror" rows="5" ></textarea>
        @error('comment')
           <span class="invalid-feedback" role="alert">
                 <strong>{{ $message }}</strong>
          </span>
        @enderror
    </div>
    
        <button type="submit" class="btn btn" > Add Comment</button>
</form>
