<div class="row" id="comments">
        <div class="col-md-12">
    
            <div class="card card text-left " >

                <div class="card-header card-header-rose">
                  <h6>
                      Comments
                      
                  </h6>  
                </div>
                <div class="card-body">

                        @foreach ($video->comments as $comment)
                        <div class="row">
                            
                            <div class="col-md-8">

                             <i class="fa fa-commenting" aria-hidden="true"></i>:
                             <a href="{{ route('front.profile' , ['id' => $comment->user->id , 'slug' => slug($comment->user->name)]) }}">
                                  {{$comment->user->name}}
                            </a>
                            </div>

                            <div class="col-md-4 text-right">

                                   <span>{{$comment->created_at}}</span> 
                                      
                            </div>
                           
                    
                        </div>

                        <p>{{$comment->comment}}</p>
                        @if(auth()->user())

                            @if((auth()->user()->group=='admin') || auth()->user()->id ==$comment->user_id )
                                              <a href="#" style="font-size:24px" onclick="$(this).next('div').slideToggle(1000);return false;"><i class="fa fa-edit"></i>
                                              </a>
                                            <div style="display:none">
                                            <form action="{{route('front.commentUpdate',['id'=>$comment->id])}}" method="post">
                                                   {{ csrf_field() }}
                                            <textarea name="comment"  class="form-control" rows="4"> {{$comment->comment}}</textarea>
                                                <button type="submit" class="btn">Edit</button>

                                            </form>

                                            </div>
                                            <a href="{{ route('front.DeleteComment' , ['id' => $comment->id]) }}" rel="tooltip" title="" class="btn btn-white btn-link btn-sm"
                                                data-original-title="Remove Comment">
                                                <i class="fa fa-trash" style="font-size:24px"></i>
                                           </a>
                                           
                                            

                            @endif
                        @endif
                         @if(!$loop->last)
                           <hr>

                         @endif
                        

                        @endforeach                       
              </div>
           
              </div>                

        </div>
        
    </div>