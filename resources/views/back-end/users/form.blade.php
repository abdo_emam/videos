{{ csrf_field() }}
<div class="row">
  <div class="col-md-6">
    <div class="form-group bmd-form-group">
      <label class="bmd-label-floating">User Name</label>
      <input type="text" class="form-control " name="name" value="{{isset($row)? $row->name : ''}}">
     
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group bmd-form-group">
      <label class="bmd-label-floating">Email</label>
    <input type="email" class="form-control " name="email" value="{{isset($row)? $row->email : ''}}">
  
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group bmd-form-group">
      <label class="bmd-label-floating">Password </label>
      <input type="password" class="form-control " name="password">
     
    </div>
  </div>

  @php $input = "group"; @endphp
    <div class="col-md-6">
        <div class="form-group bmd-form-group">
            <label class="bmd-label-floating">User Group</label>
            <select name="{{$input}}"  class="form-control  @error($input) is-invalid @enderror">
                <option value="admin" {{ isset($row) && $row->{$input} == 'admin' ? 'selected'  :'' }}>admin</option>
                <option value="user" {{ isset($row) && $row->{$input} == 'user' ? 'selected'  :'' }}>user</option>
            </select>
            @error($input)
            <span class="invalid-feedback" role="alert">
                   <strong>{{ $message }}</strong>
             </span>
            @enderror
        </div>
</div>