<div class="row">
        <div class="col-lg-12 col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Latest Comments</h4>
              <p class="card-category">New employees on 15th September, 2016</p>
            </div>
            <div class="card-body table-responsive">
              <table class="table table-hover">
                <thead class="text-warning">
                  <tr><th>ID</th>
                  <th>Name</th>
                  <th>Video</th>
                  <th>Comment</th>
                  <th>Date</th>

                </tr></thead>
                <tbody>
                 @foreach($comments as $comment)
                  <tr>
                  <td>{{$comment->id}}</td>
              {{-- {{dd($comment->id)}} --}}
                    <td>{{$comment->user->name}}</td>
                    <td>{{$comment->video->name}}</td>

                    <td>{{$comment->comment}}</td>
                    <td>{{$comment->created_at}}</td>
                 @endforeach
                 
                 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      
      </div>