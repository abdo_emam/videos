@extends('back-end.layout.app')

@section('title')
    Home Page
@endsection

@section('content')
@include('back-end.homepage-sections.statistics')
@include('back-end.homepage-sections.latestComments')


   
@endsection