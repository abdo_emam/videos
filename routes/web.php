<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('frontend.landing');

Route::namespace('BackEnd')->prefix('admin')->middleware('admin')->group(function () {

    Route::get('home', 'home@index')->name('admin.home');
    Route::resource('users', 'Users')->except(['show','delet']);
    Route::resource('categories', 'Categories')->except(['show']);
    Route::resource('skills', 'Skills');
    Route::resource('tags', 'Tags')->except(['show']);
    Route::resource('pages', 'Pages')->except(['show']);
    Route::resource('videos', 'Videos')->except(['show']);
    Route::post('comments','Videos@commentStore')->name('comments.store');

});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('category/{id}', 'HomeController@category')->name('front.category');
Route::get('skill/{id}', 'HomeController@skills')->name('front.skill');
Route::get('tag/{id}', 'HomeController@tags')->name('front.tag');
Route::get('video/{id}', 'HomeController@video')->name('front.video');
Route::get('/','HomeController@welcome')->name('frontend.landing');

Route::post('comments/{id}/add', 'HomeController@commentStore')->name('front.AddComment');
Route::post('comments/{id}', 'HomeController@commentUpdate')->name('front.commentUpdate');
Route::get('comments/{id}/delet', 'HomeController@deletComment')->name('front.DeleteComment');

Route::get('contact-us', 'HomeController@messageStor')->name('contact.store');

Route::get('profile/{id}/{slug?}','HomeController@profile')->name('front.profile');
Route::post('profile','HomeController@profileUpdate')->name('profile.update');




